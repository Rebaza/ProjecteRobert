#PRACTICA CONEXIÓN A OTRO PC MEDIANTE SSH                    by: *Robert Rebaza*
En ocasiones, en nuestro futuro trabajo de informáticos, requeriremos de una conexión telemática a otro servidor o PC de manera no presencial desde la comodidad de nuestro ordenador.
Para ello, requeriremos de una conexión SSH "cliente-servidor", aunque sólo seamos nosotros el que realize la actividad, ya que el servidor seguirá haciendo su trabajo.
Empezaremos sabiendo el comando de instalación del software en nuestro pc. (Si Joni, lo pongo pero para recordar, aunque es muy obvio, *que lo es*, a estas alturas del curso.)

*En caso de Ubuntu y derivados*
```sh
# sudo apt-get install 
```
*En caso de Fedora y derivados*
```sh
#sudo yum install 
```

##Organización de un SSH
El SSH se estructura, por decirlo de una manera, de "dos bloques" *Según Joni*, que se traduce en lo siguiente:
La ejecución de los servicios los realiza el servidor, obviamente, pudiendo nosotros ejecutar comandos sobre él mediante el mencionado SSH. 
Sin embargo, la parte de su procesado gráfico e impresión en pantalla la realiza el cliente (faltaría más, como para darle más trabajo al servidor, 
aunque tiene una explicación, no por gusto es así), ya que es el cliente el que accede al servidor y tiene la capacidad de imprimir en entorno gráfico
la informació que transmite el servidor. El cliente envía el comando, el servidor lo ejecuta y envía los resultados de vuelta al cliente.
###Aspectos a destacar 
En el caso de que el servidor al cual queremos acceder no tenga *X* (Entorno gráfico), que es con lo que nos encontraremos en la mayor parte de ocasiones, esta opción es muy viable.
Nos puede ocurrir que no dispongamos de software preinstalado en el servidor, como ahora el gparted o el gedit por poner algún ejemplo.
Se requiere que dicho software esté instalado para que lo ejecute el servidor para sus propios fines y nosotros, como clientes, acceder a la información emitida.